﻿using PokemonGoBotVer3_0.MessagesFunctional;
using PokemonGoBotVer3_0.MessagesFunctional.MainFeature;
using PokemonGoBotVer3_0.MessagesFunctional.MainFeature.AccessPolicy;
using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using System.Configuration;

namespace PokemonGoBotVer3_0
{
    class Program
    {
        private static readonly string token = ConfigurationManager.AppSettings.Get("ApiToken");
        private static readonly TelegramBotClient Bot = new TelegramBotClient(token);
        public static async Task Main()
        {
            var me = await Bot.GetMeAsync();
            Console.Title = me.Username;

            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.StartReceiving();
            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();
            Bot.StopReceiving();
            Console.WriteLine("Hello World!");
        }
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            if (message == null) 
                return;
            if (message.Type == MessageType.ChatMembersAdded)
            {
                await Welcome.WelcomeAsync(message, Bot);
                try
                {
                    CreateNewPoketrainersGuest.CheckPoketrainerExist(message.From);
                }
                catch (Exception er)
                {
                    Console.WriteLine($"{er.Message} for {message.From.Id}");
                }
            }
            else if (CreateNewPoketrainersGuest.CheckPoketrainerExist(message.From))
            {
                if (message.Type != MessageType.Text)
                    return;
                await MessagesSelectorFunction.SelectorFunctionAsync(message, Bot);
            }
            else
            {
                Console.WriteLine($"Что-то пошло не так");
            }
        }
    }
}
