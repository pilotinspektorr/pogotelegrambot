﻿using Microsoft.EntityFrameworkCore;
using PokemonGoBotVer3_0.Entitys;
using System.Configuration;

namespace PokemonGoBotVer3_0.Database
{
    class PoGoDbContext : DbContext
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["Product"].ToString();
        public DbSet<Poketrainer> Poketrainers { get; set; }
        public DbSet<PikachuPlayer> PikachuPlayers { get; set; }
        public DbSet<PokemonOfTheDay> PokemonOfTheDays { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseNpgsql(connectionString);
        }
    }
}
