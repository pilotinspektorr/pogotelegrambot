﻿namespace PokemonGoBotVer3_0.Enums
{
    public enum AccessRight
    {
        Blocked,
        Guest,
        User,
        Admin
    }
}
