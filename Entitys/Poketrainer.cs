﻿using PokemonGoBotVer3_0.Enums;
using System.ComponentModel.DataAnnotations;

namespace PokemonGoBotVer3_0.Entitys
{
    public class Poketrainer
    {
        [Key]
        public int Id { get; set; }
        public int TelegramId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string PokemonGoNickname { get; set; }
        public AccessRight Access { get; set; } = AccessRight.Guest;
        public Team Team { get; set; } = Team.None;
        public string FriendCode { get; set; } = "Не указал свой код";
        public string ReasonBanned { get; set; } = "none";
    }
}
