﻿using System;

namespace PokemonGoBotVer3_0.Entitys
{
    public class PokemonOfTheDay
    {
        public int Id { get; set; }
        public bool PlayedToday { get; set; } = false;
        public DateTime TimeOfPlaying { get; set; } = DateTime.Now;
        public short LastWinnerId { get; set; } = 0;
    }
}
