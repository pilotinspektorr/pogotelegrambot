﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PokemonGoBotVer3_0.Migrations
{
    public partial class main : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PikachuPlayers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TelegramId = table.Column<short>(nullable: false),
                    CountWin = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PikachuPlayers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PokemonOfTheDays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PlayedToday = table.Column<bool>(nullable: false),
                    TimeOfPlaying = table.Column<DateTime>(nullable: false),
                    LastWinnerId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PokemonOfTheDays", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Poketrainers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TelegramId = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    PokemonGoNickname = table.Column<string>(nullable: true),
                    Access = table.Column<int>(nullable: false),
                    Team = table.Column<int>(nullable: false),
                    FriendCode = table.Column<string>(nullable: true),
                    ReasonBanned = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Poketrainers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PikachuPlayers");

            migrationBuilder.DropTable(
                name: "PokemonOfTheDays");

            migrationBuilder.DropTable(
                name: "Poketrainers");
        }
    }
}
