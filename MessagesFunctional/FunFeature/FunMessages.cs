﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace PokemonGoBotVer3_0.MessagesFunctional.FunFeature
{
    class FunMessages
    {
        public static async Task PressFtoPayRespectAsync(Message message, ITelegramBotClient telegramBot)
        {
            var chatId = message.Chat.Id;
            try
            {
                await telegramBot.SendStickerAsync(
                        chatId: message.Chat,
                        sticker: "CAADAgADrwADTptkAoftMsdli6fnAg");
            }
            catch(Exception er)
            {
                Console.WriteLine($"Ошибка {er.Message} для {chatId}");
            }
        }
    }
}
