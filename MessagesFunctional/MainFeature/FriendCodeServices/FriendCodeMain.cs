﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Entitys;
using PokemonGoBotVer3_0.Enums;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature.FriendCodeServices
{
    class FriendCodeMain
    {
        /// <summary>
        /// Устанавливает френд-код игрока
        /// </summary>
        /// <param name="message">Сообщение которое содержит в себе команду "код" и сам френд-код</param>
        /// <param name="telegramBot">Интерфейс бота</param>
        /// <returns></returns>
        public static async Task SetMyFriendCodeAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            string friendCode = message.Text.Split(' ').Last();
            var chatId = message.Chat.Id;
            var poketrainer = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            if (friendCode.Length != 12)
            {
                try
                {
                    await telegramBot.SendTextMessageAsync(chatId, $"Длина кода больше или меньше 12 символов", replyToMessageId: message.MessageId);
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                }
            }
            else
            {
                poketrainer.FriendCode = friendCode;
                db.SaveChanges();
                try
                {
                    await telegramBot.SendTextMessageAsync(chatId, $"Установил френд-код для тебя {friendCode}", replyToMessageId: message.MessageId);
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                }
            }
        }
        public static async Task SendOneCodeByNicknameAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var poketrainer = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            var chatId = message.Chat.Id;
            var trainerSearchNickname = message.Text.Split(' ').Last().ToLower();
            var trainerSearchEntity = db.Poketrainers.FirstOrDefault(p => p.PokemonGoNickname == trainerSearchNickname);
            if(trainerSearchEntity == null)
            {
                await telegramBot.SendTextMessageAsync(chatId, $"Я такого пользователя увы не знаю", replyToMessageId: message.MessageId);
            }
            else
            {
                if (poketrainer.Access == AccessRight.Blocked || poketrainer.Access == AccessRight.Guest)
                {
                    try
                    {
                        await telegramBot.SendStickerAsync(chatId,
                        sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                        replyToMessageId: message.MessageId); //access denied
                    }
                    catch (Exception er)
                    {
                        Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                    }
                }
                else
                {
                    try
                    {
                        await telegramBot.SendTextMessageAsync(message.From.Id, 
                            $"{GetLinkToPlayerAndCode(trainerSearchEntity)}", 
                            parseMode: ParseMode.Markdown);
                        await telegramBot.SendTextMessageAsync(chatId, $"Выслал код игрока в личку",
                              replyToMessageId: message.MessageId);
                    }
                    catch (Exception er)
                    {
                        Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                        await telegramBot.SendTextMessageAsync(chatId, $"Возможно я у тебя заблокирован или остановлен, " +
                              $"зайди ко мне в личные сообщения и отправь /start для отправки списка друзей",
                              replyToMessageId: message.MessageId);
                    }
                }
            }
        }
        private static string GetLinkToPlayerAndCode(Poketrainer poketrainer)
        {
            var sb = new StringBuilder();
            if (poketrainer.UserName.Length > 0)
            {
                sb.Append($"@{poketrainer.UserName} - {poketrainer.FriendCode}");
            }
            else
            {
                sb.Append($"[{poketrainer.FirstName} {poketrainer.LastName}](tg://user?id={poketrainer.TelegramId}) - {poketrainer.FriendCode}");
            }
            return sb.ToString();
        }

        public static async Task SendFriendCodeListAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var chatId = message.Chat.Id;
            var poketrainer = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            if (poketrainer == null)
            {
                Console.WriteLine($"Пользователя нет в базе данных");
            }
            else
            {
                if(poketrainer.Access == AccessRight.Blocked || poketrainer.Access == AccessRight.Guest)
                {
                    try
                    {
                        await telegramBot.SendStickerAsync(chatId,
                        sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                        replyToMessageId: message.MessageId); //access denied
                    }
                    catch (Exception er)
                    {
                        Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                    }
                }
                else
                {
                    try
                    {
                        await SendFriendMessageAsync(message.From.Id, telegramBot);
                        await telegramBot.SendTextMessageAsync(chatId, $"Выслал список друзей тебе в личку",
                              replyToMessageId: message.MessageId);
                    }
                    catch (Exception er)
                    {
                        Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                        await telegramBot.SendTextMessageAsync(chatId, $"Возможно я у тебя заблокирован или остановлен, " +
                              $"зайди ко мне в личные сообщения и отправь /start для отправки списка друзей",
                              replyToMessageId: message.MessageId);
                    }
                }
            }
        }
        private static async Task SendFriendMessageAsync(long chatId, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var sb = new StringBuilder();
            var friendCodeList = db.Poketrainers.Where(p => p.FriendCode != "Не указал свой код").OrderBy(p => p.PokemonGoNickname).ToList();
            if(friendCodeList.Count > 40)
            {
                int indexCount = 0;
                foreach(Poketrainer u in friendCodeList)
                {
                    if(indexCount < 20)
                    {
                        sb.AppendLine($"{u.PokemonGoNickname} - {u.FriendCode} {TeamEmoji(u.Team)}");
                        indexCount++;
                    }
                    else
                    {
                        indexCount = 0;
                        sb.AppendLine($"{u.PokemonGoNickname} - {u.FriendCode} {TeamEmoji(u.Team)}");
                        await telegramBot.SendTextMessageAsync(chatId, sb.ToString());
                        sb.Clear();
                    }
                }
            }
            else
            {
                foreach (Poketrainer u in friendCodeList)
                {
                    sb.AppendLine($"{u.PokemonGoNickname} - {u.FriendCode} {TeamEmoji(u.Team)}");
                }
                await telegramBot.SendTextMessageAsync(chatId, sb.ToString());
            }
        }
        private static string TeamEmoji(Team team)
        {
            string faction = string.Empty;
            switch(team)
            {
                case Team.Cheater:
                    {
                        faction = "☠️";
                    }
                    break;
                case Team.None:
                    {
                        faction = "💚";
                    }
                    break;
                case Team.Valor:
                    {
                        faction = "❤️";
                    }
                    break;
                case Team.Mystic:
                    {
                        faction = "💙";
                    }
                    break;
                case Team.Instinct:
                    {
                        faction = "💛";
                    }
                    break;
            }
            return faction;
        }
    }
}



