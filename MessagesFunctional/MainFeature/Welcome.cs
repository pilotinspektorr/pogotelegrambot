﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature
{
    class Welcome
    {
        public static async Task WelcomeAsync(Message message, ITelegramBotClient telegramBot)
        {
            try
            {
                string newUser = Convert.ToString(message.NewChatMembers[0]);
                newUser = newUser.Split(' ').First();
                await telegramBot.SendTextMessageAsync(chatId: message.Chat, "Добро пожаловать " + newUser +
                    " в Тверской чат Pokemon Go!\n" +
                    "Будь добр, пришли скриншот твоего профиля, а также помни, что в нашем коммьюнити <b>ЗАПРЕЩЕНО</b>" +
                    " использовать fake gps и прочие программы для подмены твоего местоположения!\n" +
                    "Если ты ничего не понимаешь, советуем почитать канал: https://t.me/pogonewbie \n",
                    parseMode: ParseMode.Html);
            }
            catch (Exception err)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(err.Message);
                Console.ResetColor();
            }
        }
    }
}
