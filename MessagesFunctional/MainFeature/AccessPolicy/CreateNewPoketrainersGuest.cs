﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Entitys;
using System;
using System.Linq;
using Telegram.Bot.Types;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature.AccessPolicy
{
    class CreateNewPoketrainersGuest
    {
        public static bool CheckPoketrainerExist(User user)
        {
            using var db = new PoGoDbContext();
            var userInDatabase = db.Poketrainers.FirstOrDefault(p => p.TelegramId == user.Id);
            if(userInDatabase != null)
            {
                return true;
            }
            else
            {
                Poketrainer poketrainer = new Poketrainer
                {
                    Id = user.Id,
                    TelegramId = user.Id,
                    UserName = user.Username,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
                try
                {
                    db.Add(poketrainer);
                    db.SaveChanges();
                    Console.WriteLine($"Создаю запись в памяти о пользователе {user.FirstName} {user.LastName} (@{user.Username})_{user.Id}");
                    return true;
                }
                catch(Exception er)
                {
                    Console.WriteLine($"Что-то пошло не так для {user.FirstName} {user.LastName} (@{user.Username})_{user.Id}");
                    Console.WriteLine($"{er.Message}");
                    Console.WriteLine($"{er.StackTrace}");
                    return false;
                }
            }
        }
    }
}
