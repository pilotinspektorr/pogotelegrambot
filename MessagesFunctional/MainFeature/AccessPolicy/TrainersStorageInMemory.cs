﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PokemonGoBotVer3_0
{
    //unuesable func
    class TrainersStorageInMemory
    {
        public static Dictionary<int, Poketrainer> Poketrainers = new Dictionary<int, Poketrainer>();
        public static void InitAllPoketrainerFromDb()
        {
            using var db = new PoGoDbContext();
            var trainersList = db.Poketrainers.Where(p => p.TelegramId > 0);
            foreach(Poketrainer u in trainersList)
            {
                Poketrainers.Add(u.TelegramId, u);
            }
            if(trainersList.Count() == Poketrainers.Count())
            {
                Console.WriteLine($"Все пользователи из БД помещены в оперативную память для дальнейшей работы");
            }
            else
            {
                Console.WriteLine($"Не все пользователи из БД помещены в оперативную память для дальнейшей работы кек");
            }
        }
        public static void SaveChangesInDatabase()
        {
            using var db = new PoGoDbContext();
            var poketrainerList = Poketrainers.Values.ToList();
            foreach(Poketrainer u in poketrainerList)
            {
                db.Update(u);
            }
            db.SaveChanges();
            Console.WriteLine($"Данные по пользователям были сохранены");
        }
    }
}
