﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Enums;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature.AccessPolicy
{
    class AccessGrantedRevoked
    {
        /// <summary>
        /// Установка прав доступа участнику чата
        /// </summary>
        /// <param name="message"> Сообщение в котором содержится команда /setuser</param>
        /// <param name="telegramBot">Интерфейс бота</param>
        /// <returns></returns>
        public static async Task GrantedAccessAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var trainerSend = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            var chatId = message.Chat.Id;
            if (trainerSend.Access == AccessRight.Admin)
            {
                //ok
                int idUserAccess = message.ReplyToMessage.From.Id;
                var trainerAccess = db.Poketrainers.FirstOrDefault(p => p.TelegramId == idUserAccess); ;
                if (trainerAccess == null)
                {
                    await telegramBot.SendTextMessageAsync(chatId, $"Этот пользователь у меня не записан, но я попробую это исправить!");
                    if (CreateNewPoketrainersGuest.CheckPoketrainerExist(message.ReplyToMessage.From))
                    {
                        trainerAccess = db.Poketrainers.FirstOrDefault(p => p.TelegramId == idUserAccess);
                        trainerAccess.Access = AccessRight.User;
                        db.SaveChanges();
                        await telegramBot.SendTextMessageAsync(chatId, $"Записал, дал доступ ;)");
                    }
                }
                else
                {
                    trainerAccess.Access = AccessRight.User;
                    db.SaveChanges();
                    await telegramBot.SendTextMessageAsync(chatId, $"Пользователь получил доступ");
                }

            }
            else
            {
                await telegramBot.SendStickerAsync(chatId,
                        sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                        replyToMessageId: message.MessageId); //access denied
            }
        }
        public static async Task MakeAdminAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var trainerSend = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            var chatId = message.Chat.Id;
            if (trainerSend.Access == AccessRight.Admin)
            {
                int idUserAccess = message.ReplyToMessage.From.Id;
                var trainerAccess = db.Poketrainers.FirstOrDefault(p => p.TelegramId == idUserAccess);
                if (trainerAccess == null)
                {
                    await telegramBot.SendTextMessageAsync(chatId, $"Этот пользователь у меня не записан, но я попробую это исправить!");
                    if(CreateNewPoketrainersGuest.CheckPoketrainerExist(message.ReplyToMessage.From))
                    {
                        trainerAccess = db.Poketrainers.FirstOrDefault(p => p.TelegramId == idUserAccess);
                        trainerAccess.Access = AccessRight.Admin;
                        db.SaveChanges();
                        await telegramBot.SendTextMessageAsync(chatId, $"Записал, дал админку ;)");
                    }
                }
                else
                {
                    trainerAccess.Access = AccessRight.Admin;
                    db.SaveChanges();
                    await telegramBot.SendTextMessageAsync(chatId, $"Пользователь получил доступ");
                }

            }
            else
            {
                await telegramBot.SendStickerAsync(chatId,
                        sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                        replyToMessageId: message.MessageId); //access denied
            }
        }
    }
}
