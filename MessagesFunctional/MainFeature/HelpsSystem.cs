﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature
{
    class HelpsSystem
    {
        public static async Task MainHelpMsgSenderAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var poketrainer = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            var chatId = message.Chat.Id;
            if (poketrainer.Access == AccessRight.Guest)
            {
                Console.WriteLine($"Received a text message in chat {message.Chat.Id}.");
                //квест яйца дитто
                await telegramBot.SendTextMessageAsync(
                    chatId: chatId,
                    text: $"Список команд данного бота:\n" +
                    $"<i>Квесты / Рейды / Яйца / Дитто</i> - скидывает почти самые актуальные инфографики на данный момент.\n" +
                    $"<b>Гайды и правила</b>: 'https://telegra.ph/Dobro-pozhalovat-dorogoj-drug-01-23'\n" +
                    $"<b>Помощь</b>: 'https://t.me/PokemonSchools'\n" +
                    $"<i>ПВП чат</i> https://t.me/PoGoPvPTver",
                    parseMode: ParseMode.Html);
            }
            else if(poketrainer.Access == AccessRight.Blocked)
            {
                try
                {
                    await telegramBot.SendStickerAsync(chatId,
                    sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                    replyToMessageId: message.MessageId); //access denied
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                }
            }
            else
            {
                Console.WriteLine($"Received a text message in chat {message.Chat.Id}.");
                //квест яйца дитто
                await telegramBot.SendTextMessageAsync(
                    chatId: chatId,
                    text:
                    $"Список команд данного бота:\n" +
                    $"<b>Установи [твой код]</b> - устанавливает твой код, как пример команда установи 123456789012 изменит мой френд код на 123456789012\n" +
                    $"<b>Кто [Игровой ник]</b> - попытается найти кого - то из списка всех игроков, которые когда-то оставили свой след.\n" +
                    $"<i>Например Кто 1Forge попытается найти тебе игрока с никнеймом 1Forge и дать её контакт в телеге.</i>\n" +
                    $"<b>Ник [Твой ник]</b> - установит твой никнейм, для поиска через команду Кто, делай его таким же как и в игре, дабы тебя могли найти)\n" +
                    $"/friends_code - команда которая скинет тебе список всех френдкодов, если у тебя конечно же есть доступ\n" +
                    $"<i>Квесты / Рейды / Яйца / Дитто</i> - скидывает почти самые актуальные инфографики на данный момент.\n" +
                    $"<b>Фракция [Valor / Mystic / Instinct] </b> - установка твоей фракции\n"+
                    $"<b>Код [Ник тренера]</b> - получить код тренера в личку\n" +
                    $"<b>Гайды и правила</b>: 'https://telegra.ph/Dobro-pozhalovat-dorogoj-drug-01-23'\n" +
                    $"<b>Помощь</b>: 'https://t.me/PokemonSchools'\n" +
                    $"<i>ПВП чат</i> https://t.me/PoGoPvPTver",
                    parseMode: ParseMode.Html);
            }
        }
    }
}
