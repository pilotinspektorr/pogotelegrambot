﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Enums;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature
{
    class SearchUserByNickname
    {
        /// <summary>
        /// Кто это? Сервис отвечающий на вопрос кто этот игрок авторизованным пользователям в системе имеющим доступ к боту
        /// </summary>
        /// <param name="message"> Сообщение пользователя </param>
        /// <param name="telegramBot"> Интерфейс бота </param>
        /// <returns></returns>
        public static async Task WhoIsItAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var poketrainer = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            var chatId = message.Chat.Id;
            if(poketrainer.Access == AccessRight.Guest || poketrainer.Access == AccessRight.Blocked)
            {
                try
                {
                    await telegramBot.SendStickerAsync(chatId,
                        sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                        replyToMessageId: message.MessageId);
                }
                catch(Exception er)
                {
                    Console.WriteLine($"{er.Message} for {poketrainer.TelegramId}");
                }
            }
            else
            {
                var poGoNickname = message.Text.Split(' ').Last().ToLower();
                var poketrainerSearch = db.Poketrainers.FirstOrDefault(p => p.PokemonGoNickname == poGoNickname);
                if(poketrainerSearch == null)
                {
                    try
                    {
                        await telegramBot.SendTextMessageAsync(chatId, $"Такого пользователя я не нашёл", replyToMessageId: message.MessageId);
                    }
                    catch (Exception er)
                    {
                        Console.WriteLine($"{er.Message} for {poketrainer.TelegramId}");
                    }
                }
                else
                {
                    if(poketrainerSearch.UserName.Length > 0)
                    {
                        var sb = new StringBuilder();
                        sb.Append($"Это @{poketrainerSearch.UserName}");
                        try
                        {
                            await telegramBot.SendTextMessageAsync(chatId, $"{sb.ToString()}", replyToMessageId: message.MessageId);
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine($"{er.Message} for {poketrainer.TelegramId}");
                        }
                    }
                    else
                    {
                        //[inline mention of a user](tg://user?id=123456789) markdown
                        var sb = new StringBuilder();
                        sb.Append($"Это [{poketrainerSearch.FirstName} {poketrainerSearch.LastName}](tg://user?id={poketrainerSearch.TelegramId})");
                        try
                        {
                            await telegramBot.SendTextMessageAsync(chatId, $"{sb.ToString()}", replyToMessageId: message.MessageId, parseMode: ParseMode.Markdown);
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine($"{er.Message} for {poketrainer.TelegramId}");
                        }
                    }
                }
            }
        }
    }
}
