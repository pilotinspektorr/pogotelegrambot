﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Entitys;
using PokemonGoBotVer3_0.Enums;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature
{
    class BanSystem
    {
        public static async Task BaningUserAsync(Message message, ITelegramBotClient telegramBot)
        {
            var fullMessage = message.Text.Split(' ');
            using var db = new PoGoDbContext();
            var userBanned = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.ReplyToMessage.From.Id);
            var user = message.ReplyToMessage.From;
            string userName = string.Empty;
            try
            {
                userName = user.FirstName + " " + user.LastName;
                if(userName == string.Empty)
                {
                    userName = user.Username;
                }
            }
            catch(Exception er)
            {
                Console.WriteLine($"{er.Message}");
                Console.WriteLine($"{er.StackTrace}");

            }
            var poketrainerWhoBanned = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            if (poketrainerWhoBanned.Access != AccessRight.Admin)
            {
                await telegramBot.SendTextMessageAsync(chatId: message.Chat, "Недостаточно прав", replyToMessageId: message.MessageId);
            }
            else
            {
                if (userBanned.Access == AccessRight.Admin)
                {
                    await telegramBot.SendTextMessageAsync(chatId: message.Chat, "Ну ты и пёс конечно, админов банить", replyToMessageId: message.MessageId);
                }
                else
                {
                    try
                    {
                        string reason = ProcessBanReason(fullMessage);
                        await telegramBot.KickChatMemberAsync(chatId: message.Chat, userBanned.TelegramId, DateTime.Now.AddDays(400));
                        await telegramBot.SendTextMessageAsync(chatId: message.Chat, "Пользователь " + userName + " отправлен в ад");
                        userBanned.Access = AccessRight.Blocked;
                        userBanned.ReasonBanned = $"{reason}";
                        db.SaveChanges();
                    }
                    catch
                    {
                        await telegramBot.SendTextMessageAsync(message.Chat,
                            "Произошла ошибка, возможно у меня не хватает силы для этого",
                            replyToMessageId: message.MessageId);
                    }
                }
            }
        }
        public static async Task ShowMeListBannedAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var poketrainers = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            if(poketrainers.Access == AccessRight.Admin)
            {
                await ProcessBanListReasonAsync(message.From.Id, telegramBot);
            }
            else
            {
                await telegramBot.SendTextMessageAsync(message.Chat.Id, $"У тебя недостаточно прав", replyToMessageId: message.MessageId);
            }
        }
        private static async Task ProcessBanListReasonAsync(long chatId, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var bannedList = db.Poketrainers.Where(p => p.Access == AccessRight.Blocked).ToList();
            var sb = new StringBuilder();
            sb.AppendLine($"Список забаненых в этом чате:");
            sb.AppendLine();
            if(bannedList.Count() > 40)
            {
                int indexCount = 0;
                foreach (Poketrainer u in bannedList)
                {
                    if(indexCount < 20)
                    {
                        sb.AppendLine($"{u.FirstName} {u.LastName} / [{u.UserName}](tg://user?id={u.TelegramId}) - забанен за: {u.ReasonBanned}");
                        indexCount++;
                    }
                    else
                    {
                        sb.AppendLine($"{u.FirstName} {u.LastName} / [{u.UserName}](tg://user?id={u.TelegramId}) - забанен за: {u.ReasonBanned}");
                        await telegramBot.SendTextMessageAsync(chatId, sb.ToString(), parseMode: ParseMode.Markdown);
                        sb.Clear();
                        indexCount = 0;
                    }
                    
                }
            }
            else
            {
                foreach(Poketrainer u in bannedList)
                {
                    sb.AppendLine($"{u.FirstName} {u.LastName} / {u.UserName} - забанен за: {u.ReasonBanned}");
                }
                await telegramBot.SendTextMessageAsync(chatId, sb.ToString(), parseMode: ParseMode.Markdown);
            }
        }
        private static string ProcessBanReason(params string [] args)
        {
            var sb = new StringBuilder();
            for(int i = 1; i < args.Length; i++)
            {
                sb.Append(args[i] + " ");
            }
            return sb.ToString();
        }
    }
}
