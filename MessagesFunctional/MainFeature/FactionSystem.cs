﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature
{
    class FactionSystem
    {
        public static async Task SetMyFactionAsync(Message message, ITelegramBotClient telegramBot)
        {
            var chatId = message.Chat.Id;
            using var db = new PoGoDbContext();
            var poketrainer = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            if(poketrainer.Access == AccessRight.Blocked)
            {
                try
                {
                    await telegramBot.SendStickerAsync(chatId,
                    sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                    replyToMessageId: message.MessageId); //access denied
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                }
            }
            else if(poketrainer.Access == AccessRight.Guest)
            {
                try
                {
                    await telegramBot.SendTextMessageAsync(chatId, $"У тебя нет доступа к этому функционалу", replyToMessageId: message.MessageId);
                    //access denied
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                }
            }
            else
            {
                try
                {
                    var faction = message.Text.Split(' ').Last().ToLower();
                    Team team = Team.None;
                    switch (faction)
                    {
                        case "mystic":
                            {
                                team = Team.Mystic;
                            }
                            break;
                        case "valor":
                            {
                                team = Team.Valor;
                            }
                            break;
                        case "instinct":
                            {
                                team = Team.Instinct;
                            }
                            break;
                    }
                    poketrainer.Team = team;
                    db.SaveChanges();
                    await telegramBot.SendTextMessageAsync(chatId, $"Выставил твою фракцию на {faction}!", replyToMessageId: message.MessageId);
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} для {poketrainer.TelegramId}");
                }
            }
        }
    }
}
