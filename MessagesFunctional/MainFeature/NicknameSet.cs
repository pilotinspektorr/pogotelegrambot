﻿using PokemonGoBotVer3_0.Database;
using PokemonGoBotVer3_0.Enums;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace PokemonGoBotVer3_0.MessagesFunctional.MainFeature
{
    class NicknameSet
    {
        public static async Task NicknameSetAsync(Message message, ITelegramBotClient telegramBot)
        {
            using var db = new PoGoDbContext();
            var poketrainers = db.Poketrainers.FirstOrDefault(p => p.TelegramId == message.From.Id);
            var chatId = message.Chat.Id;
            if(poketrainers.Access == AccessRight.Guest || poketrainers.Access == AccessRight.Blocked)
            {
                try
                {
                    await telegramBot.SendStickerAsync(chatId, 
                        sticker: "CAACAgIAAxkBAAJh8l65tKbVCjETchd3PCbuOEGCH4Z5AAK8AgAC4_ATDMpKckuwg893GQQ",
                        replyToMessageId: message.MessageId); //access denied
                }
                catch(Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} for {poketrainers.TelegramId}");
                }
            }
            else
            {
                var nickName = message.Text.Split(' ').Last().ToLower();
                var sb = new StringBuilder();
                sb.Append($"Твой никнейм: {nickName} успешно задан!");
                poketrainers.PokemonGoNickname = nickName;
                db.SaveChanges();
                try
                {
                    await telegramBot.SendTextMessageAsync(chatId, $"{sb.ToString()}", replyToMessageId: message.MessageId);
                }
                catch (Exception er)
                {
                    Console.WriteLine($"Ошибка {er.Message} for {poketrainers.TelegramId}");
                }
            }
        }
    }
}
