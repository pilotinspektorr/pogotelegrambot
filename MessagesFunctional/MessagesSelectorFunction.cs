﻿using PokemonGoBotVer3_0.MessagesFunctional.FunFeature;
using PokemonGoBotVer3_0.MessagesFunctional.MainFeature;
using PokemonGoBotVer3_0.MessagesFunctional.MainFeature.AccessPolicy;
using PokemonGoBotVer3_0.MessagesFunctional.MainFeature.FriendCodeServices;
using System;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace PokemonGoBotVer3_0.MessagesFunctional
{
    class MessagesSelectorFunction
    {
        public static async Task SelectorFunctionAsync(Message message, ITelegramBotClient telegramBot)
        {
            switch (message.Text.Split(' ').First().ToLower())
            {
                case "/ban":
                    {
                        await BanSystem.BaningUserAsync(message, telegramBot);
                    }
                    break;
                case "/restrict":
                    {
                        await BanSystem.ShowMeListBannedAsync(message, telegramBot);
                    }
                    break;
                case "/promote":
                    {
                        await AccessGrantedRevoked.MakeAdminAsync(message, telegramBot);
                    }
                    break;
                case "/setuser":
                    {
                        await AccessGrantedRevoked.GrantedAccessAsync(message, telegramBot);
                    }
                    break;
                case "установи":
                    {
                        await FriendCodeMain.SetMyFriendCodeAsync(message, telegramBot);
                    }
                    break;
                case "кто":
                    {
                        await SearchUserByNickname.WhoIsItAsync(message, telegramBot);
                    }
                    break;
                case "код":
                    {
                        await FriendCodeMain.SendOneCodeByNicknameAsync(message, telegramBot);
                    }
                    break;
                case "ник":
                    {
                        await NicknameSet.NicknameSetAsync(message, telegramBot);
                    }
                    break;
                case "яйца":
                    {
                        try
                        {
                            await telegramBot.SendTextMessageAsync(message.Chat.Id, $"Актуальный список дропа из яиц: https://leekduck.com/eggs/");
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine($"{er.Message} for {message.Chat.Id}");
                        }
                    }
                    break;
                case "квесты":
                    {
                        try
                        {
                            await telegramBot.SendTextMessageAsync(message.Chat.Id, $"Актуальный список квестов: https://leekduck.com/research/");
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine($"{er.Message} for {message.Chat.Id}");
                        }
                    }
                    break;
                case "дитто":
                    {
                        try
                        {
                            await telegramBot.SendTextMessageAsync(message.Chat.Id, $"Где можно найти дитто: https://leekduck.com/FindDitto/");
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine($"{er.Message} for {message.Chat.Id}");
                        }
                    }
                    break;
                case "рейды":
                    {
                        try
                        {
                            await telegramBot.SendTextMessageAsync(message.Chat.Id, $"Актуальный список рейд боссов: https://leekduck.com/boss/");
                        }
                        catch (Exception er)
                        {
                            Console.WriteLine($"{er.Message} for {message.Chat.Id}");
                        }
                    }
                    break;
                case "/friends_code":
                    {
                        await FriendCodeMain.SendFriendCodeListAsync(message, telegramBot);
                    }
                    break;
                case "/friends_code@poketver_bot":
                    {
                        await FriendCodeMain.SendFriendCodeListAsync(message, telegramBot);
                    }
                    break;
                case "/f":
                    {
                        await FunMessages.PressFtoPayRespectAsync(message, telegramBot);
                    }
                    break;
                case "/f@poketver_bot":
                    {
                        await FunMessages.PressFtoPayRespectAsync(message, telegramBot);
                    }
                    break;
                case "/start":
                    {
                        await telegramBot.SendTextMessageAsync(
                          chatId: message.Chat.Id,
                          text: $"Привет, я PokeTverBot и постараюсь вам помочь разобраться в игре Pokemon Go! ^^");
                    }
                    break;
                case "/start@poketver_bot":
                    {
                        await telegramBot.SendTextMessageAsync(
                          chatId: message.Chat.Id,
                          text: $"Привет, я PokeTverBot и постараюсь вам помочь разобраться в игре Pokemon Go! ^^");
                    }
                    break;
                case "фракция":
                    {
                        await FactionSystem.SetMyFactionAsync(message, telegramBot);
                    }
                    break;
                case "/helps":
                    {
                        await HelpsSystem.MainHelpMsgSenderAsync(message, telegramBot);
                    }
                    break;
                case "да":
                    {
                        await CuntRandom.DaPizdaAsync(message, telegramBot);
                    }
                    break;
            }
        }
    }
}
